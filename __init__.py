# -*- coding: utf-8 -*-

#******************************************************************************
#
# Data Explorer
# ---------------------------------------------------------
# Provides tools to statistical data exploration
#
# Copyright (C) 2014 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************


from __future__ import absolute_import
def name():
    return "Data Explorer"


def description():
    return "Data analysis and visualization for a field"


def category():
    return "Vector"


def version():
    return "1.0.1"


def qgisMinimumVersion():
    return "1.8.0"


def author():
    return "Iacopo Borsi"


def email():
    return "iacopo.borsi@tea-group.com"


def icon():
    return "icons/dataexp.png"


def classFactory(iface):
    from qgis.PyQt.QtWidgets import QMessageBox

    wnd = iface.mainWindow()

    from .dataexplorer import DataExpPlugin
    return DataExpPlugin(iface)
