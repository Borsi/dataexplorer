# -*- coding: utf-8 -*-
#
#******************************************************************************


from __future__ import absolute_import
from future import standard_library
standard_library.install_aliases()
import os
import configparser

from qgis.PyQt.QtCore import QLocale, QSettings, QUrl
from qgis.PyQt.QtWidgets import QDialog, QAction, QFileDialog, QMessageBox, QDialogButtonBox
from qgis.PyQt.QtGui import QPixmap, QTextDocument, QIcon, QDesktopServices

from .ui.ui_aboutdialogbase import Ui_Dialog

from . import resources


class AboutDialog(QDialog, Ui_Dialog):
    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)

        self.btnHelp = self.buttonBox.button(QDialogButtonBox.Help)

        cfg = configparser.SafeConfigParser()
        cfg.read(os.path.join(os.path.dirname(__file__), "metadata.txt"))
        version = cfg.get("general", "version")

        self.lblLogo.setPixmap(QPixmap(":/icons/dataexp.png"))
        self.lblVersion.setText(self.tr("Version: %s") % (version))
        doc = QTextDocument()
        doc.setHtml(self.getAboutText())
        self.textBrowser.setDocument(doc)
        self.textBrowser.setOpenExternalLinks(True)

        self.buttonBox.helpRequested.connect(self.openHelp)

    def reject(self):
        QDialog.reject(self)

    def openHelp(self):
        overrideLocale = bool(QSettings().value("locale/overrideFlag", False))
        if not overrideLocale:
            localeFullName = QLocale.system().name()
        else:
            localeFullName = QSettings().value("locale/userLocale", "")

        localeShortName = localeFullName[0:2]
        if localeShortName in ["ru", "uk"]:
            QDesktopServices.openUrl(QUrl("http://www.tea-group.com"))
        else:
            QDesktopServices.openUrl(QUrl("http://www.tea-group.com"))

    def getAboutText(self):
        return self.tr('<p>Provides tools for statistical data exploration, stored as  \
            field of vector layer. </p>\
            <p><strong>Help/Suggestions</strong>:</p>\
            <p>Data Explorer is aimed at plotting and/or analysing \
            data series, with particular enphasis on time series. \
            The plugin structure has been partially mutuated by the plugin \
            <b>Statist</b>: the User is encouradged to use also that plugin \
            to complete the analysis of its data. <br>\
            With <b>Data Explorer</b> you can: <br>\
            -Plot data and compare them with their descriptive parameters <br>\
            -Compare different data using multi-plot or scatter plot.<br>\
            -Perform Normality tests: Normal q-q plot;\
            Kolmogorov-Smirnov test.<br>\
            -Perform Time Series Analysis (autocorrelation, periodogramm, trend test) <br> </p>\
            <p><strong>Developer</strong>: Iacopo Borsi</p>\
            <p><strong>Homepage</strong>: <a href="http://www.tea-group.com">http://www.tea-group.com</a></p>\
            <p><strong>Aknowledgement</strong>: Development of this plugin has been co-financed by the EU FP7-ENV-2013-WATER-INNO-DEMO MARSOL. MARSOL project receives funding from the EU 7h Framework Programme for Research, Technological Development and Demonstration under grant agreement n. 619120 (<a href="http://marsol.eu">http://marsol.eu</a>)</p></p>\
            '
            )
