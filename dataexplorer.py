# -*- coding: utf-8 -*-

#******************************************************************************
#
# Data Explorer
# ---------------------------------------------------------
# Provides tools to statistical data exploration
#
# Copyright (C) 2014 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************


from __future__ import absolute_import
from builtins import str
from builtins import object
from qgis.PyQt.QtCore import QFileInfo, QSettings, QCoreApplication
from qgis.PyQt.QtWidgets import QFileDialog, QAction
from qgis.PyQt.QtGui import QPixmap, QTextDocument, QIcon, QDesktopServices
from qgis.core import *

from . import dataexplorerdialog
from . import aboutdialog
from . import dataexplorer_utils as utils

from . import resources


class DataExpPlugin(object):
    def __init__(self, iface):
        self.iface = iface


        # For i18n support
##        userPluginPath = QFileInfo(QgsApplication.qgisUserDbFilePath()).path() + "/python/plugins/dataexplorer"
##        systemPluginPath = QgsApplication.prefixPath() + "/python/plugins/dataexplorer"

##        overrideLocale = bool(QSettings().value("locale/overrideFlag", False))
##        if not overrideLocale:
##            localeFullName = QLocale.system().name()
##        else:
##            localeFullName = QSettings().value("locale/userLocale", "")

##        if QFileInfo(userPluginPath).exists():
##            translationPath = userPluginPath + "/i18n/dataexplorer_" + localeFullName + ".qm"
##        else:
##            translationPath = systemPluginPath + "/i18n/dataexplorer_" + localeFullName + ".qm"

##        self.localePath = translationPath
##        if QFileInfo(self.localePath).exists():
##            self.translator = QTranslator()
##            self.translator.load(self.localePath)
##            QCoreApplication.installTranslator(self.translator)

    def initGui(self):
        self.actionRun = QAction(QCoreApplication.translate("DataExp", "DataExp"), self.iface.mainWindow())
        self.iface.registerMainWindowAction(self.actionRun, "Shift+D")
        self.actionRun.setIcon(QIcon(":/plugins/dataexplorer/icons/dataexp.png"))
        self.actionRun.setWhatsThis("Explore data of a field")
        self.actionAbout = QAction(QCoreApplication.translate("DataExp", "About dataexplorer..."), self.iface.mainWindow())
        self.actionAbout.setIcon(QIcon(":/plugins/dataexplorer/icons/about.png"))
        self.actionAbout.setWhatsThis("About Data Explorer")


        self.iface.addPluginToMenu(QCoreApplication.translate("DataExp", "DataExp"), self.actionRun)
        self.iface.addPluginToMenu(QCoreApplication.translate("DataExp", "DataExp"), self.actionAbout)
        self.iface.addVectorToolBarIcon(self.actionRun)

        self.actionRun.triggered.connect(self.run)
        self.actionAbout.triggered.connect(self.about)


    def unload(self):
        self.iface.unregisterMainWindowAction(self.actionRun)

        self.iface.removeVectorToolBarIcon(self.actionRun)
        self.iface.removePluginVectorMenu(QCoreApplication.translate("DataExp", "DataExp"), self.actionRun)
        self.iface.removePluginVectorMenu(QCoreApplication.translate("DataExp", "DataExp"), self.actionAbout)

    def run(self):
        layersCount = len(utils.getVectorLayerNames())
        if layersCount == 0:
            self.iface.messageBar().pushMessage(QCoreApplication.translate("DataExp", "At least one layer should be loaded"))
            return

        d = dataexplorerdialog.DataExpDialog(self.iface)
        d.show()
        d.exec_()

    def about(self):
        d = aboutdialog.AboutDialog()
        d.exec_()
